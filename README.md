ADMIN CAN LOGIN ANYUSER
------------

INTRODUCTION
------------
The Admin can login any user is a module that allows you to login from admin by
just one click,
and you can back to login previous logged in username. This module to use for
Development WORK. When we need to login multiple role user . it is hard to
remember credentials. this module provide to way logged in user without putting
and credential.


REQUIREMENTS
------------
Drupal 8.x

INSTALLATION
------------

Install the ADMIN CAN LOGIN ANYUSER:
  Using DRUSH: drush en admin_can_login_anyuser
  -or-
  Download it from https://www.drupal.org/project/admin_can_login_anyuser and
  install it to your website.

CONFIGURATION
-------------
  a) Go to admin/people/permissions. Please enable two permission of
   "Authenticated user" User
     i)-> admin login back.
     ii)-> admin switch login.
